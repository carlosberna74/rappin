import React from "react";

import Navbar from "./components/Navbar";
import VistaAdmin from "./components/VistaAdmin";
import AgregarProductos from './components/AgregarProductos';
import Productos from './components/Productos'
import {UsuarioContext} from './context/UsuarioProvider'

const App = () => {
  const {usuario} = React.useContext(UsuarioContext)
  return (
    <div>
      <Navbar />
      <div className="container">
        {
          usuario.rol === 'admin' &&  <VistaAdmin/>
        }
        {
          usuario.rol === 'provedor' &&  <AgregarProductos/>
        }
      
         <Productos/>
       </div>
    </div>
  );
};

export default App;
