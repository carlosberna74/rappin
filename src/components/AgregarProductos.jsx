import React from "react";

import { db } from "../firebase";

import { UsuarioContext } from "../context/UsuarioProvider";
import { PrductosContext } from "../context/ProductosProvider";

const AgregarProductos = () => {
  const [name, setName] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [price, setPrice] = React.useState("");
  const [image, setImage] = React.useState("");

  const { usuario } = React.useContext(UsuarioContext);
  const { fetchProductos } = React.useContext(PrductosContext);

  const agregarProduc = (e) => {
    e.preventDefault();
    if (!name.trim() || !description.trim()) {
      console.log("campos vacios");
      return;
    }
    db.collection("productos").add({
        description: description,
        image: image,
        name: name,
        price: price,
        uid: usuario.uid,
      })
      .the((doc) => {
        console.log(doc);
        console.log(doc);
        fetchProductos();
      })
      .catch((error) => console.log(error));
      setName('')
      setDescription('')
      setPrice('')
  };

  return (
    <div className='mt-5'>
        <h3>Agregar Productos </h3>
      <from onSubmit={agregarProduc}>
        <input
          type="text"
          className="form-control mb-2"
          placeholder="Ingresa nombre"
          onChange={(e) => setName(e.target.value)}
          value={name}
        />

        <input
          type="text"
          className="form-control mb-2"
          placeholder="Ingresa descripcion"
          onChange={(e) => setDescription(e.target.value)}
          value={description}
        />

        <input
          type="text"
          className="form-control mb-2"
          placeholder="Ingresa precio"
          onChange={(e) => setPrice(e.target.value)}
          value={price}
        />
        <input
          type="text"
          className="form-control mb-2"
          placeholder="Ingresa imagen"
          onChange={(e) => setImage(e.target.value)}
          value={image}
        />

        <button type="submit" className="btn btn-primary">
          Agregar
        </button>
      </from>
    </div>
  );
};

export default AgregarProductos;
