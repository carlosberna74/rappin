import React from 'react';

import {PrductosContext} from '../context/ProductosProvider'
import PintarProducto from './PintarProducto';

const Productos = () => {

  const {productos} = React.useContext(PrductosContext)
   
  return (
        <div className='mt-5'>
          <h3>Lista de productos</h3>
     <ul className="list-group">{
            productos.map(producto =>(
              <li className='list-group-item' key={producto.id}>
                   <span>{producto.name}</span> 
                   
              </li>
            ))
          }
          
          </ul> 
        </div>
    )
}

export default Productos
