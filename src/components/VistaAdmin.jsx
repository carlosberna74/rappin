import React from "react";
import { db, functions } from "../firebase";

const VistaAdmin = () => {
  const [usuarios, setUsuarios] = React.useState([]);
  React.useEffect(() => {
    fetchUsuarios();
  }, []);
  const fetchUsuarios = async () => {
    try {
      const res = await db.collection("usuarios").get();
      const arrayUsuarios = res.docs.map((doc) => doc.data());
      setUsuarios(arrayUsuarios);
    } catch (error) {
      console.log(error);
    }
  };
  const administrador = (email) => {
    if (!email.trim()) {
      return console.log("email vacio");
    }
    const agregarRol = functions.httpsCallable("agregarAdmin");
    agregarRol({ email: email }).then((res) => {
      console.log(res);
      if (res.data.error) {
        console.log("no tiene permisos");
        return;
      }
      db.collection("usuarios")
        .doc(email)
        .update({ rol: "admin" })
        .then((user) => {
          console.log("usuario modificado de rol");
          fetchUsuarios();
        });
    });
  };

  const crearProvedor = (email) => {
    const agregarRol = functions.httpsCallable("crearProvedor");
    agregarRol({ email: email }).then((res) => {
      console.log(res);
      if (res.data.error) {
        console.log("no tiene permisos");
        return;
      }
      db.collection("usuarios")
        .doc(email)
        .update({ rol: "provedor" })
        .then((user) => {
          console.log("usuario modificado de rol provedor");
          fetchUsuarios();
        });
    });
  };
  const eliminarProvedor = (email) => {
    const agregarRol = functions.httpsCallable("eliminarProvedor");
    agregarRol({ email: email }).then((res) => {
      console.log(res);
      if (res.data.error) {
        console.log("no tiene permisos");
        return;
      }
      db.collection("usuarios")
        .doc(email)
        .update({ rol: "invitado" })
        .then((user) => {
          console.log("usuario modificado de rol provedor");
          fetchUsuarios();
        });
    });
  };

  const eliminarAdmin = (email) => {
    const agregarRol = functions.httpsCallable("eliminarAdmin");
    agregarRol({ email: email }).then((res) => {
      console.log(res);
      if (res.data.error) {
        console.log("no tiene permisos");
        return;
      }
      db.collection("usuarios")
        .doc(email)
        .update({ rol: "invitado" })
        .then((user) => {
          console.log("usuario modificado de rol Invitado");
          fetchUsuarios();
        });
    });
  };

  return (
    <div>
      <h3>Administracion de usuarios</h3>
      {usuarios.map((usuario) => (
        <div key={usuario.uid} className="mb-2">
          {usuario.email} -rol:{usuario.rol}
          {usuario.rol === "admin" ? (
            <button
              type="button"
              className="btn  btn-danger mx-2 "
              onClick={() => eliminarAdmin(usuario.email)}
            >
             Eliminar Admin
            </button>
          ) : (
            <>
              <button
                type="button"
                className="btn btn-dark mx-2"
                onClick={() => administrador(usuario.email)}
              >
                Administrador
              </button>
              <button
                type="button"
                className="btn btn-success mx-2"
                onClick={() => crearProvedor(usuario.email)}
              >
                Provedor
              </button>
              <button
                type="button"
                className="btn btn-info mx-2"
                onClick={() => eliminarProvedor(usuario.email)}
              >
                Invitado
              </button>
            </>
          )}
        </div>
      ))}
    </div>
  );
};

export default VistaAdmin;
