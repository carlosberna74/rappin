import React from "react";
import {db} from '../firebase'

export const PrductosContext = React.createContext();

const ProductosProvider = (props) => {
    const [productos, setProductos] = React.useState([])

React.useEffect(()=>{
    fetchProductos()
},[])

const fetchProductos = async()=>{
    try{
const res = await db.collection('productos').get()
const arrayProductos = res.docs.map(doc => {
    return{
        ...doc.data(),
        id: doc.id
    }
})
setProductos(arrayProductos)
    }
  catch (error){
    console.log(error)
}
}
  return (
      <PrductosContext.Provider value={{productos,fetchProductos}}>
          {props.children}
      </PrductosContext.Provider>
  )
}

export default ProductosProvider;
