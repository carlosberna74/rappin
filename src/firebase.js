import firebase from 'firebase/app'
import 'firebase/firebase-firestore'
import 'firebase/auth'
import 'firebase/functions'

const firebaseConfig={
  
  apiKey: "AIzaSyDGaWLeZTyIbHMu5CFrDG8CcrrA0qd4vKA",
  authDomain: "fire-472c4.firebaseapp.com",
  databaseURL: "https://fire-472c4.firebaseio.com",
  projectId: "fire-472c4",
  storageBucket: "fire-472c4.appspot.com",
  messagingSenderId: "976247034818",
  appId: "1:976247034818:web:685efbcca0ce598a677c5b",
  measurementId: "G-ZYGVCP29P0"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  const db=firebase.firestore()
  const auth = firebase.auth()
const functions = firebase.functions()
  export{db,auth,firebase,functions}