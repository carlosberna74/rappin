import React from "react";
import ReactDOM from "react-dom";
import App from "./App.jsx";

import UsuarioProvider from "./context/UsuarioProvider";
import ProductosProvider from "./context/ProductosProvider";

ReactDOM.render(
  <React.StrictMode>
    <UsuarioProvider>
      <ProductosProvider>
        <App />
      </ProductosProvider>
    </UsuarioProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
